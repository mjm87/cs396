﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterScript))]
public class WalkScript : MonoBehaviour
{


    private Animator animator;

    public float forwardSpeed = 1f;
    public float rotationSpeed = 1f;
    public float spinSpeed = 5f;               // turn on a dime speed 
    public float dashingMultiplier = 2f;

    private bool moving = false;
    private bool dashing = false;

    // animation run speeds
    private const float IDLING = 0f;
    private const float WALKING = 1f;
    private const float DASHING = 2f;

    private CharacterScript character;

    // Use this for initialization
    void Start()
    {
        animator = transform.GetComponentInChildren<Animator>();
        character = GetComponent<CharacterScript>();
    }

    // Update is called once per frame
    void Update()
    {

        // determining direction / speed
        Vector3 dir = Vector3.forward * Input.GetAxis("Vertical");
        if (Input.GetAxis("Vertical") <= 0.1f) dir = Vector3.zero;                 // disabling backwards movement
        float movementSpeed = forwardSpeed * Time.deltaTime;

        // determining if we're moving or not
        if (Input.GetAxis("Vertical") > 0.1f) moving = true;
        else moving = false;

        // updating whether or not we're dashing
        if (!dashing && Input.GetKeyDown(KeyCode.LeftShift)) dashing = true;
        if (dashing && Input.GetKeyUp(KeyCode.LeftShift)) dashing = false;

        // adding in dashing
        if (dashing) movementSpeed *= dashingMultiplier;

        // if(Input.GetAxis("Vertical") < -0.1f) transform.Rotate(Vector3.up, spinSpeed);
        if (Input.GetAxis("Vertical") < -0.1f) transform.Rotate(Vector3.up, Input.GetAxis("Vertical") * spinSpeed);


        // Update position / rotation
        // TODO: consider adding physics based movement at some point
        transform.Translate(dir * movementSpeed);
        transform.Rotate(Vector3.up, Input.GetAxis("Horizontal") * rotationSpeed);

        // Updating animator component with speed to trigger walk/run animations
        if (moving && dashing) animator.SetFloat("Speed", DASHING);
        else if (moving) animator.SetFloat("Speed", WALKING);
        else animator.SetFloat("Speed", IDLING);

        // Developer Commands
        if( Input.GetKeyDown(KeyCode.LeftAlt)) {
            // Ground character if flying
            transform.position = new Vector3(transform.position.x, 0f, transform.position.z);
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            GetComponent<Rigidbody>().angularVelocity = Vector3.zero; 
        }
        

    }
}