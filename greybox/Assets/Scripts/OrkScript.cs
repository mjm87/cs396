﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// extra imports
using UnityEngine.AI;
using UnityEngine.Events;



// GRAMMAR CAVEAT:
// Yes I know the proper spelling of orc is well orc...
// However we're dealing with a pig-orc (orc-pig?), so ork felt more appropriate.

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class OrkScript : MonoBehaviour {


	private NavMeshAgent agent;
	private Animator animator;

	[SerializeField]
	private Transform target;


	public float maxRange = 10f;

	private UnityEvent TargetFound = new UnityEvent();
    private UnityEvent TargetLost = new UnityEvent();

	private GameObject GameStateManager;

	private bool chasingPlayer = false;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent>();
		animator = GetComponent<Animator>();

		GameStateManager = GameObject.Find("GameStateManager");

		// check if the target is in range every second
		StartCoroutine(TrackTarget(1f));
		TargetFound.AddListener(OnTargetFound);
        TargetLost.AddListener(OnTargetLost);

		chasingPlayer = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(agent.velocity.magnitude > 0f) animator.SetFloat("speed", 2f);
    }

	void OnTriggerEnter(Collider enemy){
		if(enemy.tag == "Player"){
			endGame();
		}
	}

	void OnTargetFound() {
		
		// check if scavenger is in line of sight
		RaycastHit hit;
		Transform head = transform.Find("Head");
		Vector3 targetPosition = target.Find("ScavengerObject/Head").position;
		if(Physics.Raycast(
			origin:  head.position, 
			direction: targetPosition - head.position, 
			hitInfo: out hit
		)){
			// check that we hit the scavenger
			if(hit.transform == target) {

				// We've found the target!
				// Debug.Log("We found him!");

				// Turn towards scavenger
				transform.LookAt(target);	
				
				// Move towards target's current position (last known position)
				agent.destination = target.position;

				chasingPlayer = true;

			}
            else
            {
                // we lost the target
                TargetLost.Invoke();
				chasingPlayer = false;
            }
		} else
        {
            Debug.Log("we didn't find anything at all? What?");
        }
	}

    void OnTargetLost()
    {
        agent.destination = transform.position;
        animator.SetFloat("speed", 0f);
    }


	void endGame(){
		GameStateManager.GetComponent<GameStateScript>().EndGame();
	}

	IEnumerator TrackTarget(float frequency){
		while(true) {
			float distanceToTarget = (target.position - transform.position).magnitude;
            if (distanceToTarget <= maxRange) TargetFound.Invoke();
            else TargetLost.Invoke();
			yield return new WaitForSeconds(frequency);
		}
	}
}
