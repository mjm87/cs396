﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootableScript : MonoBehaviour, Interactable {

	//public Item items; // string for now?
	private Bag<Item> bag;
	public List<ItemType> lootItems;

	public List<Transform> lootPopupPrefabs = new List<Transform>();

	private int lootQuantity = -1;
	private List<Item> lootableLootItems;

    public void Interact(CharacterScript character)
    {
		popupLootDialogue();
    }

	public void Remove(Item item) {
		lootableLootItems.Remove(item);
		lootQuantity--;
		popupLootDialogue();
	}

	private void popupLootDialogue(){
		// create the dialogue popup box above the item
		if(lootQuantity >= 0) {
			
			// actually create the loot prefab 
			Transform popup = Instantiate<Transform>(
				lootPopupPrefabs[lootQuantity], 			// corresponding to the number of loot items
				transform.position + Vector3.up * 3f,	// placing it above the lootable chest/locker/whatever 
				Quaternion.identity);

			// rotated towards camera
			popup.GetComponent<LookAt>().target = Camera.main.transform;

			// Pass the data to the loot dialogue
			popup.GetComponent<LootDialogueScript>().AddData(lootableLootItems);
			popup.GetComponent<LootDialogueScript>().lootableScript = this;



		}
	}
	

    // Use this for initialization
    void Start () {

		// populate loot bag
		bag = new Bag<Item>();
		foreach(ItemType item in lootItems){
			bag.Add(item.item, item.count);
		}

		// populating locker with up to 3 random loot items
		lootQuantity = Random.Range(0,4);
		if(lootItems.Count <= 0) lootQuantity = 0; 
		lootableLootItems = new List<Item>();
		for(int i = 0; i < lootQuantity; i++) {
			lootableLootItems.Add(bag.PickRandomItem());
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

[System.Serializable]
public struct ItemType {
	public Item item;
	public int count;
}

[System.Serializable]
public class Bag<T> {

	List<T> items;

	public Bag() {
		items = new List<T>();
	}

	public Bag(IEnumerable<T> items) {
		this.items = new List<T>(items);
	}

	// Check if the bag is empty
	public bool IsEmpty(){
		return items.Count <= 0;
	}

	// Put a new item into the bag
	public void Add(T item) {
		items.Add(item);
	}

	// Add a number of items
	public void Add(T item, int quantity){
		for(int i = 0; i < quantity; i++){
			items.Add(item);
		}
	}

	// Pick a random item without replacing
	public T Remove() {
		T picked = pickARandomItem();
		items.Remove(picked);	
		return picked;
	}

	// Pick a random item with replacing
	public T PickRandomItem() {
		T picked = pickARandomItem();
		return picked;
	}

	private T pickARandomItem(){
		throwErrorIfBagIsEmpty();
		T picked = default(T);
		int randomIndex = Random.Range(0,items.Count);
		picked = items[randomIndex];
		return picked;
	}

	// only meant to be called when trying to draw from the bag
	// when the bag is empty
	private void throwErrorIfBagIsEmpty(){
		if(IsEmpty()) {
			throw new System.Exception("InvalidOperationException: Bag is empty");
		}	
	}
}