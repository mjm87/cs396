﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class GameStateScript : MonoBehaviour {

	public GameObject GameOverUI;
    public GameObject InventoryUI;

	public void EndGame(){
		GameOverUI.SetActive(true);
		Time.timeScale = 0f;
        // turn off inventory UI if it's up
        InventoryUI.SetActive(false);
	}

	public void RemoveGameOverOverlay(){
		GameOverUI.SetActive(false);
		Time.timeScale = 1f;
	}


    public void Restart(){
        RemoveGameOverOverlay();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }   

    public void QuitGame(){
        // UnityEditor.EditorApplication doesn't exist in builds, throwing building errors
        //if(Application.isEditor) UnityEditor.EditorApplication.isPlaying = false;
        Application.Quit();
    }

    void Update() {
        // TODO: create a pause menu at some point
        if(Input.GetKeyDown(KeyCode.Escape)){
            EndGame();
        }
    }
}
