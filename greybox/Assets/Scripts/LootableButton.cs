﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootableButton : MonoBehaviour {


	public LootDialogueScript lootDialogueScript;
	private InventoryDisplay inventory;
	private Item item;

	// Use this for initialization
	void Start () {
		inventory = GameObject.Find("InventoryManager").GetComponent<InventoryDisplay>();
		if(GetComponent<UnityEngine.UI.Button>() == null) gameObject.AddComponent<UnityEngine.UI.Button>();
		GetComponent<UnityEngine.UI.Button>().onClick.AddListener(onClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseOver() {
		// popup hover box showing item description???
	}

	void OnMouseDown() {
		inventory.Add(item);
		// remove item from dialogue
		// OR grey item from dialouge
		// OR recreate popup with reduced data?
	}

	void onClicked() {
		inventory.Add(item);
		lootDialogueScript.Remove(item);
	}


	public void AddData(Item item) {
		this.item = item;
		GetComponent<UnityEngine.UI.Image>().sprite = item.image;
	}


}
