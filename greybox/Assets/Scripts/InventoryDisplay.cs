﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

public class InventoryDisplay : MonoBehaviour {

    public List<GameObject> itemSlots = new List<GameObject>();

    public List<Item> items = new List<Item>();

    private GameObject inventoryUI;

	// Use this for initialization
	void Start () {
		inventoryUI = GameObject.Find("GameStateManager").GetComponent<GameStateScript>().InventoryUI;
	}
	
	// Update is called once per frame
	void Update () {

        // Toggle Inventory visibility by pressing I
        if(Input.GetKeyDown(KeyCode.I)){
            inventoryUI.SetActive(!inventoryUI.activeInHierarchy);
            UpdateGUI();
        }

	}

    // re-flash
    void UpdateGUI(){

        // clear all item slots real quickly
        foreach(GameObject go in itemSlots) go.SetActive(false);

        // re-add any items to the inventory
        for(int i = 0; i < items.Count; i++) {
            itemSlots[i].SetActive(true);
            itemSlots[i].GetComponent<Image>().sprite = items[i].image;
        }
    }

    // Add item
    public void Add(Item item){
        if(thereIsAnOpenSlot()) {
            inventoryUI.SetActive(true);
            items.Add(item);
            UpdateGUI();
        }
    }

    public void RemoveAt(int itemIndex) {
        // check that index is positive
        if(itemIndex >= 0) {
            // check that index is within limits of our total inventory
            if(itemIndex < items.Count){
                // actually remove the item at that index
                // DROPPING it with no hope of recovery... (could eventually do something else...)
                items.RemoveAt(itemIndex);
                UpdateGUI();
            }
        }
    }

    private bool thereIsAnOpenSlot() {
        return items.Count < itemSlots.Count;
    }
}
