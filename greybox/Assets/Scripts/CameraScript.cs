﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public Transform focus;

	public float minCloseUp = 3f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        transform.LookAt(focus);

        Vector3 scrolling = Vector3.forward * Input.GetAxis("Mouse ScrollWheel") * 50f;

		// if we're scrolling inward
		if(Input.GetAxis("Mouse ScrollWheel") > 0) {
			float offset = (focus.position - transform.position).magnitude;
			// and we're too close
			if(offset <= minCloseUp) {
				// prevent further zooming in
				scrolling = Vector3.zero;
			}
		}

		// scroll in our out
        transform.Translate(scrolling * Time.deltaTime);
	}
}
