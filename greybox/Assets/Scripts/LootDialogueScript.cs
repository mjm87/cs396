﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootDialogueScript : MonoBehaviour {

	private List<Transform> popups = new List<Transform>();

	public LootableScript lootableScript;

	private bool readyToProcessData = false;

	// Use this for initialization
	void Start () {
		readyToProcessData = false;
		foreach(Transform child in transform) {
			if(child.GetComponent<LootableButton>() != null) {
				popups.Add(child);	
				child.GetComponent<LootableButton>().lootDialogueScript = this;
			}
		}	
		readyToProcessData = true;
	}

	// Update is called once per frame
	void Update () {
		if(Input.anyKeyDown) {
			if(isNotAValidKeyPress())
			GameObject.Destroy(gameObject);			
		}	
	}

	public void Remove(Item item) {
		lootableScript.Remove(item);
		GameObject.Destroy(gameObject);
	}


	public void AddData(List<Item> lootItems) {
		if(lootItems.Count > 0) {
			IEnumerator createListItems = createButtonsFromData(lootItems);
			StartCoroutine(createListItems);
		}
	}

	private bool isNotAValidKeyPress() {
		return 
			!Input.GetKeyDown(KeyCode.A) && 
			!Input.GetKeyDown(KeyCode.D) && 
			!Input.GetKeyDown(KeyCode.LeftArrow) && 
			!Input.GetKeyDown(KeyCode.RightArrow) &&
			!Input.GetMouseButtonDown(0);
	}

	private IEnumerator createButtonsFromData(List<Item> lootItems) {

		// wait until we're ready to process data
		yield return new WaitUntil(() => readyToProcessData);

		if(popups.Count > 0) {

			// process data
			for(int i = 0; i < lootItems.Count; i++) {
				popups[i].GetComponent<LootableButton>().AddData(lootItems[i]);
				//popups[i].GetComponent<UnityEngine.UI.Image>().sprite = lootItems[i].item.image;
			}
		}

	}

	 
}
