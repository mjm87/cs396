﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour {

	public Transform target;

	// Use this for initialization
	void Start () {
		lookAtTarget();
	}

	void Update () {
		lookAtTarget();
	}

	private void lookAtTarget() {
		if(target != null) {
			transform.LookAt(target, Vector3.up);
			transform.Rotate(Vector3.up, 180);
		}		
	}
}
